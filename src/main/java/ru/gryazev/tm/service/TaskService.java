package ru.gryazev.tm.service;

import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.util.ArrayUtils;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class TaskService{

    private TaskRepository taskRepository;

    private ProjectRepository projectRepository;

    public TaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public void create(String projectId, Task task) throws IOException {
        if (!"".equals(projectId))
            task.setProjectId(projectRepository.findOne(projectId).getId());
        taskRepository.persist(task);
    }

    public Task view(String taskId) {
        return taskRepository.findOne(taskId);
    }

    public List<Task> list(String projectId) {
        return taskRepository.findAll().values().stream().filter(o ->
                o.getProjectId().equals(projectId)).collect(Collectors.toList());
    }

    public void edit(Task task) {
        taskRepository.merge(task);
    }

    public boolean remove(String taskId) {
        return (taskRepository.remove(taskId) != null);
    }

    public void clear() {
        taskRepository.removeAll();
    }

    public String getTaskId(String projectId, int taskIndex) {
        List<Task> tasks = taskRepository.findAll().values().stream().filter(o ->
                o.getProjectId().equals(projectId))
                .collect(Collectors.toList());

        if (!ArrayUtils.indexExists(tasks, taskIndex))
            return null;

        return tasks.get(taskIndex).getId();
    }

}