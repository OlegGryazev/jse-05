package ru.gryazev.tm.service;

import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.util.ArrayUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProjectService{

    private ProjectRepository projectRepository;

    private TaskRepository taskRepository;

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public void create(Project project) throws IOException {
        projectRepository.persist(project);
    }

    public Project view(String projectId) {
        return projectRepository.findOne(projectId);
    }

    public List<Project> list() {
        return new ArrayList<>(projectRepository.findAll().values());
    }

    public void edit(Project project) {
        projectRepository.merge(project);
    }

    public boolean remove(String projectId) {
        if (projectRepository.remove(projectId) == null)
            return false;
        for (Map.Entry<String, Task> pair : taskRepository.findAll().entrySet())
            if (pair.getValue().getProjectId().equals(projectId))
                taskRepository.remove(pair.getValue().getId());
        return true;
    }

    public void clear(){
        projectRepository.removeAll();
        taskRepository.removeAll();
    }

    public String getProjectId(int projectIndex){
        List<Project> projects = new ArrayList<>(projectRepository.findAll().values());

        if (!ArrayUtils.indexExists(projects, projectIndex))
            return "";

        return projects.get(projectIndex).getId();
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

}
