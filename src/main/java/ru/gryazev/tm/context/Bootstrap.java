package ru.gryazev.tm.context;

import ru.gryazev.tm.command.*;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.service.ProjectService;
import ru.gryazev.tm.service.TaskService;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Bootstrap {

    private Map<String, AbstractCommand> commands = new HashMap<>();

    private ConsoleView consoleView = new ConsoleView();

    private ProjectRepository projectRepository = new ProjectRepository();

    private TaskRepository taskRepository = new TaskRepository();

    private ProjectService projectService = new ProjectService(projectRepository, taskRepository);

    private TaskService taskService = new TaskService(projectRepository, taskRepository);

    private String selectedProjectId = "";

    public void init() {
        consoleView.print("**** Welcome to Project Manager ****");
        commands = commandsInit();
        try{
            while (true) {
                AbstractCommand command = commands.get(consoleView.readCommand());
                if (command == null) {
                    consoleView.print("Command not found!");
                    continue;
                }
                command.execute();
            }
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            consoleView.close();
        }
    }

    private Map<String, AbstractCommand> commandsInit() {
        Map<String, AbstractCommand> result = new HashMap<>();

        AbstractCommand[] commandArray = new AbstractCommand[]{new ExitCommand(this), new HelpCommand(this), new ProjectClearCommand(this),
                new ProjectCreateCommand(this), new ProjectEditCommand(this), new ProjectListCommand(this), new ProjectRemoveCommand(this),
                new ProjectSelectCommand(this), new ProjectViewCommand(this), new TaskClearCommand(this), new TaskClearCommand(this),
                new TaskCreateCommand(this), new TaskEditCommand(this), new TaskListCommand(this), new TaskRemoveCommand(this), new TaskViewCommand(this)};

        for (AbstractCommand abstractCommand : commandArray) {
            result.put(abstractCommand.getName(), abstractCommand);
        }
        return result;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ConsoleView getConsoleView() {
        return consoleView;
    }

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    public String getSelectedProjectId() {
        return selectedProjectId;
    }

    public void setSelectedProjectId(String selectedProjectId) {
        this.selectedProjectId = selectedProjectId;
    }

}
