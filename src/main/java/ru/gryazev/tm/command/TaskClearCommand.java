package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;

public class TaskClearCommand extends AbstractCommand {

    public TaskClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Clear tasks list";
    }

    @Override
    public void execute() {
        bootstrap.getTaskService().clear();
    }

}
