package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;

import java.io.IOException;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;

    public AbstractCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws IOException;

}
