package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;

public class ProjectClearCommand extends AbstractCommand {

    public ProjectClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Clear projects list";
    }

    @Override
    public void execute() {
        bootstrap.getProjectService().clear();
    }

}
