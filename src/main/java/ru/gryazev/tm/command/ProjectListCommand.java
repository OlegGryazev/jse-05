package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.view.ConsoleView;

import java.util.List;

public class ProjectListCommand extends AbstractCommand {

    public ProjectListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        ConsoleView consoleView = bootstrap.getConsoleView();
        List<Project> projects = bootstrap.getProjectService().list();
        if (projects.size() == 0){
            consoleView.print("[LIST IS EMPTY]");
        }
        for (int i = 0; i < projects.size(); i++)
            consoleView.print((i + 1) + ". " + projects.get(i).getName());
    }

}
