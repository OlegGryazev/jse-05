package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.service.ProjectService;
import ru.gryazev.tm.view.ConsoleView;

public class ProjectSelectCommand extends AbstractCommand {

    public ProjectSelectCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-select";
    }

    @Override
    public String getDescription() {
        return "Select project. To reset type \"-1\"";
    }

    @Override
    public void execute() {
        ConsoleView consoleView = bootstrap.getConsoleView();
        ProjectService projectService = bootstrap.getProjectService();
        int projectIndex = consoleView.getProjectIndex();
        String projectId = projectService.getProjectId(projectIndex);
        bootstrap.setSelectedProjectId(projectId == null ? "" : projectId);
    }

}
