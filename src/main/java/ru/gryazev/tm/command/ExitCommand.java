package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;

public class ExitCommand extends AbstractCommand {

    public ExitCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit from program.";
    }

    @Override
    public void execute() {
        bootstrap.getConsoleView().close();
        System.exit(0);
    }

}
