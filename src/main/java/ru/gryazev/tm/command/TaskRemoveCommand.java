package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.service.ProjectService;
import ru.gryazev.tm.service.TaskService;
import ru.gryazev.tm.view.ConsoleView;

public class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task from selected project.";
    }

    @Override
    public void execute() {
        ConsoleView consoleView = bootstrap.getConsoleView();
        TaskService taskService = bootstrap.getTaskService();
        int taskIndex = consoleView.getTaskIndex();
        String taskId = taskService.getTaskId(bootstrap.getSelectedProjectId(), taskIndex);
        if (taskService.remove(taskId))
            consoleView.print("[DELETED]");
    }

}
