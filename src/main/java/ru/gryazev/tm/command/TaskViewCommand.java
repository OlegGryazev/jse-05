package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.service.TaskService;
import ru.gryazev.tm.view.ConsoleView;

public class TaskViewCommand extends AbstractCommand {

    public TaskViewCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-view";
    }

    @Override
    public String getDescription() {
        return "View selected task.";
    }

    @Override
    public void execute() {
        ConsoleView consoleView = bootstrap.getConsoleView();
        TaskService taskService = bootstrap.getTaskService();
        int taskIndex = consoleView.getTaskIndex();
        String taskId = taskService.getTaskId(bootstrap.getSelectedProjectId(), taskIndex);
        Task task = taskService.view(taskId);
        consoleView.print(task == null ? "[TASK NOT FOUND]" : task.toString());
    }

}
