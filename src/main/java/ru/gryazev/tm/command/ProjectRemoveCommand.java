package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.service.ProjectService;
import ru.gryazev.tm.view.ConsoleView;

public class ProjectRemoveCommand extends AbstractCommand {

    public ProjectRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        ConsoleView consoleView = bootstrap.getConsoleView();
        ProjectService projectService = bootstrap.getProjectService();
        int projectIndex = consoleView.getProjectIndex();
        String projectId = projectService.getProjectId(projectIndex);
        if (projectService.remove(projectId))
            consoleView.print("[DELETED]");
    }

}
