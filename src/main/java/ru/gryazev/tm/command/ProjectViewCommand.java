package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.service.ProjectService;
import ru.gryazev.tm.view.ConsoleView;

public class ProjectViewCommand extends AbstractCommand {

    public ProjectViewCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-view";
    }

    @Override
    public String getDescription() {
        return "View selected project.";
    }

    @Override
    public void execute() {
        ConsoleView consoleView = bootstrap.getConsoleView();
        ProjectService projectService = bootstrap.getProjectService();
        int projectIndex = consoleView.getProjectIndex();
        String projectId = projectService.getProjectId(projectIndex);
        Project project = projectService.view(projectId);
        consoleView.print(project == null ? "[PROJECT NOT FOUND]" : project.toString());
    }

}
