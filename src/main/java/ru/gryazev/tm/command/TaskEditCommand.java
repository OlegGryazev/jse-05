package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.service.ProjectService;
import ru.gryazev.tm.service.TaskService;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public class TaskEditCommand extends AbstractCommand {

    public TaskEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected task.";
    }

    @Override
    public void execute() throws IOException {
        ConsoleView consoleView = bootstrap.getConsoleView();
        TaskService taskService = bootstrap.getTaskService();
        ProjectService projectService = bootstrap.getProjectService();

        int taskIndex = consoleView.getTaskIndex();
        String taskId = taskService.getTaskId(bootstrap.getSelectedProjectId(), taskIndex);

        int projectIndex = consoleView.getProjectIndex();
        String projectId = projectService.getProjectId(projectIndex);

        Task task = consoleView.getTaskFromConsole();
        task.setId(taskId);
        task.setProjectId(projectId);

        taskService.edit(task);
        consoleView.print("[OK]");
    }

}
