package ru.gryazev.tm.view;

import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.util.DateUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public class ConsoleView {

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public Task getTaskFromConsole() throws IOException {
        Task task = new Task();
        System.out.println("[Enter name:]");
        task.setName(reader.readLine());

        System.out.println("[Enter details:]");
        task.setDetails(reader.readLine());

        System.out.println("[Enter date of task start in DD.MM.YYYY format:]");
        Date startDate = DateUtils.inputDate(reader);
        if (startDate != null)
            task.setDateStart(startDate);

        System.out.println("[Enter date of task end in DD.MM.YYYY format:]");
        Date endDate = DateUtils.inputDate(reader);
        if (endDate != null)
            task.setDateFinish(endDate);

        return task;
    }

    public Project getProjectFromConsole() throws IOException{
        Project project = new Project();
        System.out.println("[Enter name:]");
        project.setName(reader.readLine());

        System.out.println("[Enter details:]");
        project.setDetails(reader.readLine());

        System.out.println("[Enter date of project start in DD.MM.YYYY format:]");
        Date startDate = DateUtils.inputDate(reader);
        if (startDate != null)
            project.setDateStart(startDate);

        System.out.println("[Enter date of project end in DD.MM.YYYY format:]");
        Date endDate = DateUtils.inputDate(reader);
        if (endDate != null)
            project.setDateFinish(endDate);

        return project;
    }

    public int getTaskIndex() {
        return inputNumber("task index");
    }

    public int getProjectIndex() {
        return inputNumber("project index");
    }

    public String readCommand() throws IOException {
        return reader.readLine();
    }

    public void close(){
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void print(String str){
        System.out.println(str);
    }

    public int inputNumber(String source) {
        int id;
        System.out.println(String.format("[Enter %s:]", source));
        try {
            id = Integer.parseInt(reader.readLine());
        } catch (NumberFormatException | IOException e) {
            System.out.println(String.format("[Entered %s is incorrect.]", source));
            return -1;
        }
        return id - 1;
    }

}
