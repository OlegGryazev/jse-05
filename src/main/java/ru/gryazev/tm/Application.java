package ru.gryazev.tm;

import ru.gryazev.tm.context.Bootstrap;

import java.awt.image.renderable.RenderableImageProducer;

public class Application {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
