package ru.gryazev.tm.repository;

import ru.gryazev.tm.entity.Project;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    private Map<String, Project> projects = new LinkedHashMap<>();

    public void merge(Project project){
        projects.merge(project.getId(), project, (k, v) -> v);
    }

    public Map<String, Project> findAll() {
        return projects;
    }

    public Project findOne(String id){
        return projects.get(id);
    }

    public void persist(Project project) throws IOException{
        if (findOne(project.getId()) != null)
            throw new IOException("Project must have unique Id!");
        projects.put(project.getId(), project);
    }

    public Project remove(String id){
        return projects.remove(id);
    }

    public void removeAll(){
        projects.clear();
    }

}
