package ru.gryazev.tm.repository;

import ru.gryazev.tm.entity.Task;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskRepository {

    private Map<String, Task> tasks = new LinkedHashMap<>();

    public Task findOne(String id){
        return tasks.get(id);
    }

    public void merge(Task task){
        tasks.merge(task.getId(), task, (k, v) -> v);
    }

    public void persist(Task task) throws IOException{
        if (findOne(task.getId()) != null)
            throw new IOException("Project must have unique Id!");
        tasks.put(task.getId(), task);
    }

    public Task remove(String id){
        return tasks.remove(id);
    }

    public void removeAll(){
        tasks.clear();
    }

    public Map<String, Task> findAll() {
        return tasks;
    }

}
